#!/usr/bin/env bash

APP=ft.C

NP=8
smpirun -map -trace -trace-file ../../replay_to_ptask/${APP}.${NP}.paje --cfg=tracing/smpi/display-sizes:1 --cfg=tracing/smpi/internals:1 -replay ${APP}.${NP}.txt -platform ../graphene.xml -np ${NP} ../../../smpi_replay/smpi_replay ${APP}.${NP}.txt > ../../replay_to_ptask/${APP}.${NP}.out

NP=16
smpirun -map -trace -trace-file ../../replay_to_ptask/${APP}.${NP}.paje --cfg=tracing/smpi/display-sizes:1 --cfg=tracing/smpi/internals:1 -replay ${APP}.${NP}.txt -platform ../graphene.xml -np ${NP} ../../../smpi_replay/smpi_replay ${APP}.${NP}.txt > ../../replay_to_ptask/${APP}.${NP}.out

NP=32
smpirun -map -trace -trace-file ../../replay_to_ptask/${APP}.${NP}.paje --cfg=tracing/smpi/display-sizes:1 --cfg=tracing/smpi/internals:1 -replay ${APP}.${NP}.txt -platform ../graphene.xml -np ${NP} ../../../smpi_replay/smpi_replay ${APP}.${NP}.txt > ../../replay_to_ptask/${APP}.${NP}.out
