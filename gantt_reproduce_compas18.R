library(tidyverse)
library(viridis)


load_batsim_output <- function(workload_path) {
  workload <- read_csv(workload_path)
  # Data processing to have a ggplot friendly formatting
  workload_sep <- workload %>%
    # We split the jobs into one row per allocation bloque
    separate_rows(allocated_resources, sep=" ") %>%
    # We create two columns, one for the block (min resource and max)
    separate(allocated_resources, into = c("psetmin", "psetmax"), fill="right") %>%
    mutate(psetmax = as.integer(psetmax), psetmin = as.integer(psetmin)) %>%
    mutate(psetmax = ifelse(is.na(psetmax), psetmin, psetmax))
  return(workload_sep)
}

load_batsim_output_old <- function(workload_path) {
  workload <- read_csv(workload_path)
  # Data processing to have a ggplot friendly formatting
  workload_sep <- workload %>% rename(allocated_resources = "allocated_processors", requested_number_of_resources = "requested_number_of_processors") %>%
    # We split the jobs into one row per allocation bloque
    separate_rows(allocated_resources, sep=" ") %>%
    # We create two columns, one for the block (min resource and max)
    separate(allocated_resources, into = c("psetmin", "psetmax"), fill="right") %>%
    mutate(psetmax = as.integer(psetmax), psetmin = as.integer(psetmin)) %>%
    mutate(psetmax = ifelse(is.na(psetmax), psetmin, psetmax))
  return(workload_sep)
}

locality    = load_batsim_output("./experiments/simuout/sched_locality_aware/out_jobs.csv") %>%
  mutate(scheduler = "Contiguity forced", source = "Replay")
no_locality = load_batsim_output("./experiments/simuout/sched_no_locality/out_jobs.csv") %>%
  mutate(scheduler = "Contiguity not forced", source = "Replay")

locality_compas    = load_batsim_output_old("./experiments/simuout_old_results/sched_locality_aware/out_jobs.csv") %>%
  mutate(scheduler = "Contiguity forced", source = "Compas-18")
no_locality_compas = load_batsim_output_old("./experiments/simuout_old_results/sched_no_locality/out_jobs.csv") %>%
  mutate(scheduler = "Contiguity not forced", source = "Compas-18")


# Start ploting
bind_rows(locality, list(no_locality, locality_compas, no_locality_compas)) %>%
  ggplot(aes( xmin=starting_time,
              ymin=psetmin,
              ymax=psetmax + 0.9,
              xmax=finish_time,
              fill=waiting_time))+
  # We draw the rectangle
  geom_rect(alpha=0.4, color = "black", size=0.1) +
  # We add the label in the middle of the block
  geom_segment(aes(x=submission_time,
                   y=-requested_number_of_resources,
                   xend=starting_time,
                   yend=-3, color = waiting_time), size=0.1) +
  geom_point(aes(y=-requested_number_of_resources, x=submission_time, color = waiting_time), size=0.05) +
  geom_point(aes(y=-3, x=starting_time, color = waiting_time), size = 0.05) +
  theme_bw() +
  facet_grid(scheduler~source) +
  ylab("resources") + xlab("time (in seconds)")  +
  scale_color_viridis(discrete=F) + scale_fill_viridis(discrete=F) +
  ggsave("./images/replay_compas18.png", height = 8, width = 20) +
  ggsave("./images/replay_compas18.pdf", height = 8, width = 20)

