{
  hostPkgs ?
    import (fetchTarball {
      name = "pkgs";
      url = "https://github.com/NixOS/nixpkgs/archive/21.05.tar.gz";
      sha256 = "sha256:1ckzhh24mgz6jd1xhfgx0i9mijk6xjqxwsshnvq789xsavrmsc36";
    }) {},
  # Last working: f70b1149a9ae30fafe87af46e517985bacc331c3
  kapack ?
    import (fetchTarball {
      name = "kapack";
      url = "https://github.com/oar-team/kapack/archive/f70b1149a9ae30fafe87af46e517985bacc331c3.tar.gz";
      sha256 = "sha256:007xyvrcfl9qfi7947kv3clbb7wjniicn1ks6wf2dlcpd2qzmh22";
    }) {},
  nur-kapack ?
    import (fetchTarball {
      url = "https://github.com/oar-team/nur-kapack/archive/a358e6274b1159427638ab8197a7ec672a03b436.tar.gz";
      sha256 = "sha256:0qril85w591gdq1bmzh3jirwzvzp66icrcgsd5njggkdkqprvc9j";
    }) {pkgs = hostPkgs;},
}:
with kapack; let
  # jupyter = import (builtins.fetchGit {
  #   url = https://github.com/tweag/jupyterWith;
  #   rev = "21da512ee016b8374033eea7772e77cc9e21158e";
  # }) {};
  # kaPython = jupyter.kernels.iPythonWith rec {
  #   name = "kapython";
  #   python3 = pkgs.python3;
  #   packages = p: with p; [
  #     ipython
  #     aiofiles
  #     pandas
  #     numpy
  #     ruamel_yaml
  #     matplotlib
  #     exptools
  #     tzlocal
  #     simplegeneric
  #     wheel
  #     (rpy2.overrideAttrs(attrs : {
  #       buildInputs = attrs.buildInputs ++ (with pkgs.rPackages; [ tidyverse viridis ]);
  #     }))
  #   ];
  # };
  # jupyterEnvironment =
  #   jupyter.jupyterlabWith {
  #     kernels = [
  #       kaPython
  #     ];
  # }
  # Python envs for jupyter
  kernelEnv = pkgs.callPackage ./jupyterShell.nix {
    kapack = kapack;
    simgrid = nur-kapack.simgrid-330light; # Potentially not the good version QQ
    pkgs = hostPkgs;
  };
  exptools =
    import (fetchTarball {
      url = https://gitlab.inria.fr/adfaure/ptasktools/-/archive/master/ptasktools-master.tar.gz;
    }) {
      inherit pkgs;
      pythonPackages = pkgs.python37Packages;
    };
in
  pkgs.mkShell {
    buildInputs = [
      kapack.simgrid
      pkgs.boost
      batsim
      batexpe
      nur-kapack.batsched
      pkgs.zeromq
      pkgs.pkgconfig
      kernelEnv
      # simulator
      (pkgs.rstudioWrapper.override {
        packages = with pkgs.rPackages; [plotly RColorBrewer tidyverse viridis ggpubr];
      })
    ];
  }
