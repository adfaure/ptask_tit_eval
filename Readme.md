# Compas Replay

Every thing has been packaged with the packages manager Nix.
To install all the depedencies, and run the experiment and the visualisation one can use the followingg command (at the root of this directory): `nix-shell`.

This repository contains the experiment for the chapter 4 of my Phd.

To run the full experiment, one can use the notebook `Ptask_vs_tit_comparison.ipynb`.

The plot of the thesis have been generated with, `comparison_tit_ptask.R`.
