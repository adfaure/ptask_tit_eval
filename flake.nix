{
  description = "nixos-compose";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
    kapack.url = "github:oar-team/nur-kapack?ref=regale";
    kapack.inputs.nixpkgs.follows = "nixpkgs";
    jupyenv.url = "github:tweag/jupyenv";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    kapack,
    jupyenv,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};

      inherit (jupyenv.lib.${system}) mkJupyterlabNew;
      jupyterlab = mkJupyterlabNew ({
        ...
      }: {
        nixpkgs = nixpkgs;
        imports = [
          ({pkgs, ...}: {
            kernel.python.minimal = {
              enable = true;
              extraPackages = ps: [
                ps.numpy
                ps.scipy
                ps.matplotlib
                ps.ipython
                ps.aiofiles
                ps.pandas
                ps.numpy
                # ps.ruamel_yaml
                ps.pyyaml
                ps.matplotlib
                (import "/home/adfaure/code/ptasktools" {
                  inherit pkgs;
                  python3Packages = ps;
                })
                ps.tzlocal
                ps.simplegeneric
                ps.wheel
              ];
            };
          })
        ];
      });
    in rec {
      formatter = pkgs.alejandra;

      packages = {inherit jupyterlab;};
      packages.default = jupyterlab;
      apps.default.program = "${jupyterlab}/bin/jupyter-lab";
      apps.default.type = "app";

      devShell = pkgs.mkShell {
        buildInputs = with pkgs; [
          kapack.packages.${system}.batsim-410
          kapack.packages.${system}.simgrid
          kapack.packages.${system}.batexpe
          kapack.packages.${system}.batsched

          (pkgs.rstudioWrapper.override {
            packages = with pkgs.rPackages; [plotly RColorBrewer tidyverse viridis ggpubr];
          })

        ];
      };
    });
}
