library(tidyverse)
library(viridis)
library(RColorBrewer)
library(ggpubr)
require("scales")

theme_set(theme_minimal(base_size = 12))

height = 8
width = 12

load_batsim_output <- function(workload_path) {
  workload <- read_csv(workload_path)
  # Data processing to have a ggplot friendly formatting
  workload_sep <- workload %>%
    # We split the jobs into one row per allocation bloque
    separate_rows(allocated_resources, sep=" ") %>%
    # We create two columns, one for the block (min resource and max)
    separate(allocated_resources, into = c("psetmin", "psetmax"), fill="right") %>%
    mutate(psetmax = as.integer(psetmax), psetmin = as.integer(psetmin)) %>%
    mutate(psetmax = ifelse(is.na(psetmax), psetmin, psetmax))
  return(workload_sep)
}

plot_gantt_thesis <- function(data) {
  p <- data %>% ggplot(aes( xmin=starting_time,
                            ymin=psetmin,
                            ymax=psetmax + 0.9,
                            xmax=finish_time,
                            fill=waiting_time)) +
    # We draw the rectangle
    geom_rect(alpha=0.8, color = "black", size=0.1) +
    ylab("Resources") + xlab("Time (in seconds)")  +
    scale_fill_viridis_c() + scale_color_viridis_c()
  
  return(p)
}

plot_gantt <- function(data) {
  p <- data %>% ggplot(aes( xmin=starting_time,
                            ymin=psetmin,
                            ymax=psetmax + 0.9,
                            xmax=finish_time,
                            fill=waiting_time)) +
    # We draw the rectangle
    geom_rect(alpha=0.3, color = "black", size=0.1) +
    # We add the label in the middle of the block
    
    geom_segment(aes(x=submission_time,
                     y=-requested_number_of_resources,
                     xend=starting_time,
                     yend=-3, color = waiting_time), size=0.1) +
    geom_point(aes(y=-requested_number_of_resources, x=submission_time, color = waiting_time), size=0.05) +
    geom_point(aes(y=-3, x=starting_time, color = waiting_time), size = 0.05) +
    theme_bw() +
    facet_grid(model~sched) +
    ylab("Resources") + xlab("Time (in seconds)")  +
    scale_fill_viridis_c() + scale_color_viridis_c()
  
  return(p)
}


conflvls = c("Delay - Contiguity forced" = "#fde725",
             "Delay - Contiguity not forced" = "#7ad151",
             "Ptask - Contiguity forced" = "#22a884",
             "Ptask - Contiguity not forced" = "#2a788e",
             "Tit - Contiguity forced" = "#414487",
             "Tit - Contiguity not forced" = "#440154")

jColors = c("#fde725",
            "#7ad151",
            "#22a884",
            "#2a788e",
            "#414487",
            "#440154")
 
lol = read_csv("./experiments/ptask_smpi_comp/all_run_meta.csv") 
meta = read_csv("./experiments/ptask_smpi_comp/all_run_meta.csv")  %>% 
  separate(name, into = c("model", "jobs", "sched", "variability"), sep = "-") %>%
  mutate(sched = str_replace(sched, "noloc","Contiguity not forced"),
         sched = str_replace(sched, "locality", "Contiguity forced"),
         batsim_time = simulation_time ) %>%
  select(model, sched, nb_jobs_success, batsim_time) %>% 
  mutate(Configuration = factor(paste(model, sched, sep = " - "))) %>%
  group_by(Configuration, sched, model, nb_jobs_success) %>%
  summarise(mean.time = mean(batsim_time),
            sd.time = sd(batsim_time),
            n.time = n()) %>%
  mutate(se.time = sd.time / sqrt(n.time),
         lower.ci.time = mean.time - qt(1 - (0.05 / 2), n.time - 1) * se.time,
         upper.ci.time = mean.time + qt(1 - (0.05 / 2), n.time - 1) * se.time) %>%
  mutate(color = conflvls[Configuration])

# test with log
ggplot(meta, aes(x = factor(nb_jobs_success), 
                        y =  mean.time, fill = Configuration, ymin=mean.time - sd.time,
                        ymax=mean.time+sd.time)) +
  geom_bar(position = position_dodge(0.45), stat = "identity", width = 0.4) +
  geom_errorbar(position = position_dodge(0.45), width=0.4, alpha=0.9) +
  # scale_y_log10() +
  scale_y_continuous(trans='sqrt') +
  scale_fill_manual(values = jColors) + 

  # scale_fill_viridis_d() +
  theme_minimal(base_size = 18) +
  xlab("Workload size (jobs)") +
  ylab("Average simulation duration (seconds)") +
  theme(legend.position = "bottom")

           # Print simulations details
all <- ggplot(meta, aes(x = factor(nb_jobs_success), 
                 y = mean.time, fill = Configuration, ymin=mean.time - sd.time,
                 ymax=mean.time+sd.time)) +
  geom_bar(position = position_dodge(0.45), stat = "identity", width = 0.4) +
  geom_errorbar(position = position_dodge(0.45), width=0.4, alpha=0.9) +
  scale_fill_manual(values = jColors) + 
  # scale_fill_viridis_d() +
  theme_minimal(base_size = 20) +
  xlab("Workload size (jobs)") +
  ylab("Average simulation duration (seconds)") +
  theme(legend.position = "bottom") +
  ggsave("./images/simulation_time_comparison.pdf", width = 8) + guides(fill = guide_legend(ncol = 2))

all
# Print simulations details


zoom <- ggplot(meta %>% filter(model != "Tit"), aes(x = factor(nb_jobs_success), y = mean.time, fill = Configuration, ymin=mean.time - sd.time, ymax=mean.time+sd.time )) +
  geom_bar(position = position_dodge(0.45), stat = "identity", width = 0.4) +
  geom_errorbar(position = position_dodge(0.45), width=0.4, alpha=0.9) +
  scale_fill_manual(values = jColors) +
  # scale_fill_viridis_d() +
  theme_minimal(base_size = 20) +
  xlab("Workload size (jobs)") + 
  ylab("Average simulation duration (seconds)") +
  theme(legend.position = "bottom") +
  ggsave("./images/simulation_time_comparison_zoom.pdf", width = 6) + guides(fill = guide_legend(ncol = 2))

zoom

ggarrange(all, zoom,   common.legend = TRUE, legend = "bottom") +
  ggsave("images/simulation_time_comparison_with_zoom.pdf", width = 10, height = 7) +
  ggsave("/tmp/simulation_time_comparison_with_zoom.png", width = 12) +
  guides(fill = guide_legend(ncol = 2))


# Gant charts
# Load jobs
jobs = load_batsim_output("./experiments/ptask_smpi_comp/jobs_for_plot.csv") %>%
  separate(name, into = c("model", "jobs", "sched"), sep = "-") %>%
  mutate(sched = str_replace(sched, "noloc", "Contiguity not forced"), 
         sched = str_replace(sched, "locality", "Contiguity forced"))

# Thesis plots
height_thesis = 8
width_thesis= 16

global_labeller <- labeller(
  vore = capitalize,
  conservation = conservation_status,
  conservation2 = label_wrap_gen(10),
  .default = label_both
)

jobs %>% filter(jobs == "512_jobs", model == "Tit") %>%
  plot_gantt_thesis() + # ggtitle("512 Jobs Workload with Time Independant Traces") + 
  theme(legend.position = "bottom") + theme_minimal(base_size = 18) + 
  labs(fill = "Waiting Time", color = "Waiting Time") + theme(legend.position = "none") + xlim(0, 2600) +
  ggsave("./images/tit_gantt_512.pdf", height = height_thesis, width = width_thesis)

jobs %>% filter(jobs == "512_jobs", model == "Delay") %>%
  plot_gantt_thesis() + # ggtitle("512 Jobs Workload with Time Independant Traces") + 
  theme(legend.position = "bottom") + theme_minimal(base_size = 18) + 
  labs(fill = "Waiting Time", color = "Waiting Time") + theme(legend.position = "bottom")  + xlim(0, 2600) +
  ggsave("./images/delay_gantt_512.pdf", height = height_thesis, width = width_thesis)

jobs %>% filter(jobs == "512_jobs", model == "Ptask") %>%
  plot_gantt_thesis() + # ggtitle("512 Jobs Workload with Time Independant Traces") + 
  theme(legend.position = "bottom") + theme_minimal(base_size = 18) + 
  labs(fill = "Waiting Time", color = "Waiting Time") + theme(legend.position = "none")  + xlim(0, 2600) +
  ggsave("./images/ptask_gantt_512.pdf", height = height_thesis, width = width_thesis)

jobs %>% filter(jobs == "512_jobs") %>%
  plot_gantt_thesis() + # ggtitle("512 Jobs Workload with Time Independant Traces") + 
  theme(legend.position = "bottom") + theme_minimal(base_size = 18) + 
  facet_grid(model + sched ~.) +
  # facet_wrap(model + sched ~ ., ncol = 1) +   
  theme_minimal(base_size = 18) +
  labs(fill = "Waiting Time", color = "Waiting Time") + theme(legend.position = "bottom") + 
  ggsave("~/Projects/Thesis/img/gantt_512.pdf", height = 13, width = 9)


jobs %>% filter(jobs == "1024_jobs") %>%
  plot_gantt_thesis() + # ggtitle("512 Jobs Workload with Time Independant Traces") + 
  theme(legend.position = "bottom") + theme_minimal(base_size = 18) + 
  facet_grid(model + sched ~ . ) +
  # facet_wrap(model + sched ~ ., ncol = 1) +   
  labs(fill = "Waiting Time", color = "Waiting Time") + theme(legend.position = "none") +
  ggsave("./images/gantt_1024.pdf", height = 22, width = width_thesis)


# Running Time per profile
# Load jobs

jobs = load_batsim_output("./experiments/ptask_smpi_comp/jobs_for_plot.csv") %>%
  separate(name, into = c("model", "jobs", "sched"), sep = "-") %>%
  mutate(sched = str_replace(sched, "noloc", "Contiguity not forced"), 
         sched = str_replace(sched, "locality", "Contiguity forced")) %>% 
  mutate( profile = factor(profile, levels = c("ft.C.8", "ft.C.16", "ft.C.32")))
  
  
groups = jobs %>% group_by(profile, sched, model) %>% 
  # summarize(mean_exec = mean(execution_time)) %>%
  summarise(mean.time = mean(execution_time),
            sd.time = sd(execution_time),
            n.time = n()) %>%
  mutate(se.time = sd.time / sqrt(n.time),
         lower.ci.time = mean.time - qt(1 - (0.05 / 2), n.time - 1) * se.time,
         upper.ci.time = mean.time + qt(1 - (0.05 / 2), n.time - 1) * se.time)

# Waiting times
wait_den = jobs %>% mutate(profile = factor(profile, levels = c("ft.C.8", "ft.C.16", "ft.C.32"))) %>%
  ggplot(aes(x = x)) +
  geom_histogram(data = jobs %>% filter(sched != "Contiguity forced"), aes(x = waiting_time, y = ..density.., fill = sched), binwidth = 5, alpha = 0.8) +
  geom_histogram(data = jobs %>% filter(sched == "Contiguity forced"), aes(x = waiting_time, y = -..density.., fill = sched), binwidth = 5, alpha = 0.8) +
  facet_grid(model~profile, scales = "free") +
  scale_fill_viridis_d(end = 0.6) +
  xlab("Waiting Time (seconds)") +
  theme_bw(base_size = 18) + 
  theme(legend.position = "bottom") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) + 
  ggsave("./images/profiles_comparison_wtime.pdf", width = 12, height = 6)

den = jobs %>% mutate(profile = factor(profile, levels = c("ft.C.8", "ft.C.16", "ft.C.32"))) %>%
  ggplot(aes(x = x)) +
  geom_histogram(data = jobs %>% filter(sched != "Contiguity forced"), aes(x = execution_time, y = ..density.., fill = sched), binwidth = 2, alpha = 0.8) +
  geom_histogram(data = jobs %>% filter(sched == "Contiguity forced"), aes(x = execution_time, y = -..density.., fill = sched), binwidth = 2, alpha = 0.8) +
  facet_grid(model~profile, scales = "free") +
  scale_fill_viridis_d(end = 0.6) +
  theme_bw(base_size = 18) + 
  xlab("Running Time (seconds)") +
  theme(legend.position = "bottom") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

  ggarrange(den, wait_den, common.legend = TRUE, legend = "bottom", widths = c(1, 1), nrow = 2) +
  ggsave("images/profiles_comparison_w_and_e_time.pdf", width = 7, height = 9)

